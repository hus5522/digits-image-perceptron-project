﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    abstract class ALayer
    {
        public float[] Totals { get { return mTotals; } }
        public float[] Outputs { get { return mOutputs; } }
        public int NumOfPrevUnits { get { return mNumOfPrevUnits; } }
        public int NumOfUnits { get { return mNumOfUnits; } }

        private int mNumOfPrevUnits;
        private int mNumOfUnits;

        private float[] mWeights;
        private float[] mTotals;
        private float[] mOutputs;

        public ALayer(int numOfPrevUnits, int numOfUnits)
        {
            Random random = new Random();
            mNumOfPrevUnits = numOfPrevUnits;
            mNumOfUnits = numOfUnits;

            mTotals = new float[mNumOfUnits];
            mOutputs = new float[mNumOfUnits];
            mWeights = new float[mNumOfPrevUnits * mNumOfUnits];

            for(int i=0; i<mWeights.Length; i++)
            {
                mWeights[i] = (float)random.NextDouble() * 2.0f - 1.0f;
            }
        }

        /* 추가 */
        public ALayer(int numOfPrevUnits, int numOfUnits, DataControler dataControler)
        {
            mNumOfPrevUnits = numOfPrevUnits;
            mNumOfUnits = numOfUnits;

            mTotals = new float[mNumOfUnits];
            mOutputs = new float[mNumOfUnits];
            float[] mWeightOfFirst = dataControler.GetWeightOfFirst();
            float[] mWeightOfSecond = dataControler.GetWeightOfSecond();
            Console.WriteLine("mWeightOfFirst: " + dataControler.GetWeightOfFirst().Length);
            Console.WriteLine("mWeightOfSecond: " + dataControler.GetWeightOfSecond().Length);
            mWeights = new float[mNumOfPrevUnits * mNumOfUnits];

            int nextWeightIndex = 0;

            //해당 ALayer가 첫번째 레이어라면, 레이어 사이에 가중치는 존재하지 않으므로 가중치에 대한 작업을 하지 않는다.
            if (mNumOfPrevUnits == 28 * 28 && mNumOfUnits == 10 * 10)
            {   //해당 ALayer가 두번째 레이어라면, dataControler의 mWeightOfFirst를 이용한다.
                for (int i = 0; i < mNumOfUnits; i++)
                {
                    for (int j = 0; j < mNumOfPrevUnits; j++)
                    {
                        mWeights[j + i * mNumOfPrevUnits] = mWeightOfFirst[j + i * mNumOfPrevUnits];
                        nextWeightIndex++;
                    }
                }
            }
            else if(mNumOfPrevUnits == 10 * 10 && mNumOfUnits == 10)
            {   //해당 ALayer가 세번째 레이어라면, dataControler의 mWeightOfSecond를 이용한다.
                for (int i = 0; i < mNumOfUnits; i++)
                {
                    for (int j = 0; j < mNumOfPrevUnits; j++)
                    {
                        mWeights[j + i * mNumOfPrevUnits] = mWeightOfSecond[j + i * mNumOfPrevUnits];
                    }
                }
            }//if-else
        }

        /* 추가됨 for SaveData */
        public float[] GetWeights()
        {
            return mWeights;
        }

        public float GetWeight(int i, int j)
        {
            return mWeights[i + j * mNumOfPrevUnits];
        }

        public void SetWeight(int i, int j, float value)
        {
            mWeights[i + j * mNumOfPrevUnits] = value;
        }

        public void Simulate(ALayer prevLayer)
        {
            float sum;

            for(int i=0; i<mNumOfUnits; i++)
            {
                sum = 0.0f;
                for(int j=0; j<mNumOfPrevUnits; j++)
                {
                    sum += prevLayer.Outputs[j] * GetWeight(j, i);
                }

                mTotals[i] = sum;
                mOutputs[i] = Activate(sum);
            }
        }

        public abstract float Activate(float input);
        public abstract float DeltaActivate(float input);
    }
}
