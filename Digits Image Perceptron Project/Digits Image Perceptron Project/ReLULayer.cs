﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    class ReLULayer : ALayer
    {
        public ReLULayer(int numOfPrevUnits, int numOfUnits) : base(numOfPrevUnits, numOfUnits)
        {

        }

        public ReLULayer(int numOfPrevUnits, int numOfUnits, DataControler dataControler) : base(numOfPrevUnits, numOfUnits, dataControler)
        {

        }

        public override float Activate(float input)
        {
            return (input > 0.0f ? input : 0.0f);
        }
        public override float DeltaActivate(float input)
        {
            return 1.0f;
        }
    }
}
