﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    class ResultComparer
    {
        public float Accuracy
        {
            get
            {
                return (float)mNumOfCorrect / mNumOfResult;
            }
        }

        private int mNumOfResult;
        private int mNumOfCorrect;

        public ResultComparer()
        {
            mNumOfCorrect = 0;
            mNumOfResult = 0;
        }

        public void AddResult(byte realResult, byte expectedResult)
        {
            mNumOfResult++;
            if(realResult == expectedResult)
            {
                mNumOfCorrect++;
            }
        }
    }
}
