﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Digits_Image_Perceptron_Project
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            Stopwatch sw1 = new Stopwatch();
            ResultComparer comparer = new ResultComparer();
            ResultComparer randomComparer = new ResultComparer();
            Random random = new Random();
            sw.Start();
            ImageDataSet trainSet = new ImageDataSet("train-labels.idx1-ubyte", "train-images.idx3-ubyte");
            ImageDataSet testSet = new ImageDataSet("t10k-labels.idx1-ubyte", "t10k-images.idx3-ubyte");
            sw.Stop();
            //Console.WriteLine("[Read Time] : " + sw.ElapsedMilliseconds + "ms");
            sw.Reset();
            DataControler dataControler = new DataControler(AppDomain.CurrentDomain.BaseDirectory);

            string selectNum = "";
            Console.WriteLine("선택하세요\n1. 미리 학습해 놓은 가중치 사용하기\n2. 처음부터 다시 학습하기");
            selectNum = Console.ReadLine();

            MultipleLayerPerceptron perceptron;

            float[] inputs;
            float[] expectedResult = new float[10];

            switch (selectNum) {
                case "1":
                    /* 미리 학습해놓은 가중치를 사용할 때 주석 없애기 */
                    dataControler.LoadData();
                    perceptron = new MultipleLayerPerceptron(dataControler.GetNumOfLayer());
                    perceptron.SetLayer(0, new SigmoidLayer(0, 28 * 28));   //첫 레이어는 불러온 가중치를 부여할 필요 없다.
                    perceptron.SetLayer(1, new SigmoidLayer(28 * 28, 100, dataControler));
                    perceptron.SetLayer(2, new SigmoidLayer(100, 10, dataControler));
                    inputs = perceptron.Inputs;

                    for (int j = 0; j < testSet.NumOfImages; j++)
                    {
                        for (int i = 0; i < inputs.Length; i++)
                        {
                            inputs[i] = ((float)testSet.Images[j][i / 28, i % 28] / 255.0f * 1.0f - 0.5f);
                        }

                        float[] outputs = perceptron.Execute();

                        byte realResult = 0;
                        float maxPoint = 0.0f;
                        for (byte i = 0; i < 10; i++)
                        {
                            if (maxPoint < outputs[i])
                            {
                                maxPoint = outputs[i];
                                realResult = i;
                            }
                        }

                        float randomResult = (float)random.NextDouble() * 10.0f;
                        comparer.AddResult(realResult, testSet.Labels[j]);
                        randomComparer.AddResult((byte)randomResult, testSet.Labels[j]);
                        Console.WriteLine("=====\n[Real] : " + realResult + "\n[Random] : " + (byte)randomResult + "\n[Expected] : " + testSet.Labels[j]);
                    }

                    Console.WriteLine("\n[MLP Accuracy] : " + comparer.Accuracy);
                    Console.WriteLine("[Random Accuracy] : " + randomComparer.Accuracy);
                    Console.WriteLine("[Total Epoch Time] : " + sw1.ElapsedMilliseconds + "ms");
                    break;
                case "2":
                    /* 학습을 위해 사용  */
                    perceptron = new MultipleLayerPerceptron(3);
                    perceptron.SetLayer(0, new SigmoidLayer(0, 28 * 28));
                    perceptron.SetLayer(1, new SigmoidLayer(28 * 28, 100));
                    perceptron.SetLayer(2, new SigmoidLayer(100, 10));
                    inputs = perceptron.Inputs;
                    LayerTrainer trainer = new LayerTrainer(perceptron, 0.2f, 0.1f, false);

                    sw1.Start();
                    for (int j = 0; j < trainSet.NumOfImages; j++)
                    {
                        for (int i = 0; i < inputs.Length; i++)
                        {
                            inputs[i] = ((float)trainSet.Images[j][i / 28, i % 28] / 255.0f * 1.0f - 0.5f);
                        }
                        sw.Start();
                        perceptron.Execute();
                        // 0~9를 의미하는 10개의 유닛들 중, 해당되는 유닛만 1의 기댓값을 주고 나머지는 0의 기댓값을 준다
                        for (byte i = 0; i < 10; i++)
                        {
                            if (i == trainSet.Labels[j])
                            {
                                expectedResult[i] = 1f;
                            }
                            else
                            {
                                expectedResult[i] = 0f;
                            }
                        }
                        trainer.Train(expectedResult);
                        sw.Stop();
                        Console.WriteLine("[" + j + "'s Epoch Time] : " + sw.ElapsedMilliseconds + "ms");
                        sw.Reset();

                    }
                    sw1.Stop();

                    for (int j = 0; j < testSet.NumOfImages; j++)
                    {
                        for (int i = 0; i < inputs.Length; i++)
                        {
                            inputs[i] = ((float)testSet.Images[j][i / 28, i % 28] / 255.0f * 1.0f - 0.5f);
                        }

                        float[] outputs = perceptron.Execute();

                        byte realResult = 0;
                        float maxPoint = 0.0f;
                        for (byte i = 0; i < 10; i++)
                        {
                            if (maxPoint < outputs[i])
                            {
                                maxPoint = outputs[i];
                                realResult = i;
                            }
                        }

                        float randomResult = (float)random.NextDouble() * 10.0f;
                        comparer.AddResult(realResult, testSet.Labels[j]);
                        randomComparer.AddResult((byte)randomResult, testSet.Labels[j]);
                        Console.WriteLine("=====\n[Real] : " + realResult + "\n[Random] : " + (byte)randomResult + "\n[Expected] : " + testSet.Labels[j]);
                    }
                    /* 최종적으로 학습된 데이터를 세이브하고싶을때 사용 */
                    dataControler.SaveData(perceptron);
                    Console.WriteLine("\n[MLP Accuracy] : " + comparer.Accuracy);
                    Console.WriteLine("[Random Accuracy] : " + randomComparer.Accuracy);
                    Console.WriteLine("[Total Epoch Time] : " + sw1.ElapsedMilliseconds + "ms");
                    break;
                default:
                    Console.WriteLine("정확한 메뉴를 입력하세요");
                    break;
            }
        }

    }
}